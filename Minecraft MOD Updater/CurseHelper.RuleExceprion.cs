﻿using System;
using System.Collections.Generic;

namespace Minecraft_MOD_Updater
{
    public static partial class CurseHelper
    {
        public static readonly Dictionary<string, string> RuleException = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            { "Iron Chest","Iron-Chests" },
            { "What Are We Looking At","wawla-what-are-we-looking-at" },
            { "Just Enough Items","jei" },
            { "Thermal Expansion","ThermalExpansion" },
            { "Just Enough Resources","just-enough-resources-jer" },
            { "IndustrialCraft 2","industrial-craft" },
            { "EnderStorage","ender-storage-1-8" },
            { "ChickenChunks","chicken-chunks-1-8" },
            { "CodeChicken Lib","codechicken-lib-1-8" },
            { "CoFH Core","cofhcore" },
            { "CosmeticArmorReworked","cosmetic-armor-reworked" },
            { "CraftTweaker2","crafttweaker" },
            { "Lesslag","less-lag" },
            { "Waila","hwyla" },
            { "Binnie Core","binnies-mods" },
            { "Block Drops","block-drops-jei-addon" },
            { "foamfix","foamfix-for-minecraft" },
            { "Chisels & Bits","chisels-bits" },
            { "Placeable Items Mod","placeable-items" }
        };
    }
}
