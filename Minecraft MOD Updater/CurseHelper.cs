﻿using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Minecraft_MOD_Updater
{
    public static partial class CurseHelper
    {
        public static string Curseforge_URL { get => "https://minecraft.curseforge.com"; }
        static readonly string curseforge_template_filepage = Curseforge_URL + "/projects/{0}/files";
        private readonly static Regex regex_getVersion = new Regex(@"(?:(\d+)\.*)+");
        public static async Task<List<Class_VersionFilter>> GetVersionFilterListAsync(string ModName)
        {
            List<Class_VersionFilter> class_VersionsFilterList = new List<Class_VersionFilter>();
            string CurseModName_temp = "";
            CurseModName_temp = ModName.Replace(" ", "-");
            var url = String.Format(curseforge_template_filepage, CurseModName_temp);
            var web = new HtmlWeb();
            var doc = await web.LoadFromWebAsync(url);
            var allnodes = doc.DocumentNode.SelectNodes("//select[@id='filter-game-version']/option");
            if (allnodes == null)
            {
                CurseModName_temp = ModName.Replace(" ", "");
                url = String.Format(curseforge_template_filepage,CurseModName_temp );
                web = new HtmlWeb();
                doc = await web.LoadFromWebAsync(url);
                allnodes = doc.DocumentNode.SelectNodes("//select[@id='filter-game-version']/option");
                if (allnodes == null)
                {
                    //查詢RuleException
                    if(RuleException.ContainsKey(ModName))
                    {
                        CurseModName_temp = RuleException[ModName];
                        url = String.Format(curseforge_template_filepage, RuleException[ModName]);
                        web = new HtmlWeb();
                        doc = await web.LoadFromWebAsync(url);
                        allnodes = doc.DocumentNode.SelectNodes("//select[@id='filter-game-version']/option");
                        if (allnodes == null)
                        {
                            return null;
                        }
                    }
                    else
                        return null;                    
                }
            }
            allnodes.ToList().ForEach(x =>
            {
                class_VersionsFilterList.Add(new Class_VersionFilter { Version = x.InnerText.Trim(), value = x.Attributes[0].Value });
            });
            return class_VersionsFilterList;
        }
        public static async Task<List<Class_Version>> GetFileListByFilterAsync(string ModName, Class_VersionFilter class_VersionFilter)
        {
            List<Class_Version> class_Versions = new List<Class_Version>();
            var web = new HtmlWeb();
            var url = String.Format(curseforge_template_filepage, ModName.Replace(" ", "-").Replace("'",""));
            var doc = await web.LoadFromWebAsync(url + "?filter-game-version=" + class_VersionFilter.value);
            var Statslist = doc.DocumentNode.SelectNodes("//td[@class='project-file-release-type']");
            
            if (Statslist == null)
            {
                url = String.Format(curseforge_template_filepage, ModName.Replace(" ", "").Replace("'", ""));
                doc = await web.LoadFromWebAsync(url + "?filter-game-version=" + class_VersionFilter.value);
                Statslist = doc.DocumentNode.SelectNodes("//td[@class='project-file-release-type']");
                if (Statslist == null)
                {
                    if (RuleException.ContainsKey(ModName))
                    {
                        url = String.Format(curseforge_template_filepage, RuleException[ModName]);
                        doc = await web.LoadFromWebAsync(url + "?filter-game-version=" + class_VersionFilter.value);
                        Statslist = doc.DocumentNode.SelectNodes("//td[@class='project-file-release-type']");
                        if (Statslist == null)
                            return null;
                    }
                    else
                        return null;
                }
            }

            var Namelist = doc.DocumentNode.SelectNodes("//a[@class='overflow-tip twitch-link']");
            var DownloadHreflist = doc.DocumentNode.SelectNodes("//a[@class='button tip fa-icon-download icon-only']");
            for (int i = 0; i < Statslist.Count; i++)
            {
                ReleaseStats stats = ReleaseStats.Error;
                if (Statslist[i].InnerHtml.Contains("alpha-phase"))
                    stats = ReleaseStats.Alpha;
                if (Statslist[i].InnerHtml.Contains("beta-phase"))
                    stats = ReleaseStats.Beta;
                if (Statslist[i].InnerHtml.Contains("release-phase"))
                    stats = ReleaseStats.Release;
                class_Versions.Add(new Class_Version() { Stats = stats, Name = Namelist[i].InnerText, Href = Namelist[i].Attributes[1].Value });
            }
            return class_Versions;
        }        
        /// <summary>
        /// 下載MOD
        /// </summary>
        /// <param name="URL">檔案網址</param>
        /// <param name="currentPath">目前MOD路徑</param>
        /// <param name="currentVersion">目前MOD版本號</param>
        /// <param name="CheckMD5">True: 檔案不同則改名為*.disable False: 不檢查MD5直接改名</param>
        public static async System.Threading.Tasks.Task DownloadsAsync(string URL, string currentPath, string currentVersion, bool CheckMD5 = true)
        {
            var web = new HtmlWeb();
            var doc = await web.LoadFromWebAsync(URL);
            var downloadURL = await WebRequestHelper.GetTransferedURLAsync(URL + "/download", 3);
            var CurseFile_fileName = System.IO.Path.GetFileName(new Uri(downloadURL).LocalPath);
            var CurseFile_MD5 = doc.DocumentNode.SelectNodes("//span[@class='md5']").First().InnerText.ToUpper();

            string Path_MD5 = "-";
            if (CheckMD5)
            {
                FileStream file = new FileStream(currentPath, FileMode.Open);
                System.Security.Cryptography.MD5 md5 = new MD5CryptoServiceProvider();
                byte[] retVal = md5.ComputeHash(file);
                file.Close();
                Path_MD5 = BitConverter.ToString(retVal).Replace("-", "");
            }
            string tempfilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(currentPath), CurseFile_fileName + ".temp");
            if (File.Exists(tempfilePath))
                File.Delete(tempfilePath);

            Downloader.Download(URL + "/download", tempfilePath).Wait();            

            //檢查下載下來的檔案是比現在新還是比現在舊
            string tempVersion = GetVersionFromMod(tempfilePath);
            switch (WhitchIsNew(currentVersion, tempVersion))
            {
                case -1:
                    if (File.Exists(currentPath + Variable.Extenstion_Disable))
                        File.Delete(currentPath + Variable.Extenstion_Disable);
                    System.IO.File.Move(currentPath, currentPath + Variable.Extenstion_Disable);
                    File.Move(tempfilePath, tempfilePath.Substring(0, tempfilePath.Length - ".temp".Length));
                    break;
                case 0:
                    if (CheckMD5 ? CurseFile_MD5 != Path_MD5 : false)
                    {
                        if (File.Exists(currentPath + Variable.Extenstion_Disable))
                            File.Delete(currentPath + Variable.Extenstion_Disable);
                        System.IO.File.Move(currentPath, currentPath + Variable.Extenstion_Disable);
                        File.Move(tempfilePath, tempfilePath.Substring(0, tempfilePath.Length - ".temp".Length));
                    }
                    else
                        File.Delete(tempfilePath);
                    break;
                case 1:
                    File.Delete(tempfilePath);
                    break;
                case 2:
                    if (File.Exists(currentPath + Variable.Extenstion_Disable))
                        File.Delete(currentPath + Variable.Extenstion_Disable);
                    System.IO.File.Move(currentPath, currentPath + Variable.Extenstion_Disable);
                    File.Move(tempfilePath, tempfilePath.Substring(0, tempfilePath.Length - ".temp".Length));
                    break;
            }
        }
        /// <summary>
        /// 判定哪個Version比較新
        /// </summary>
        /// <param name="Version1"></param>
        /// <param name="Version2"></param>
        /// <returns>-1: 無法比較  0:相同  1:Version1比較新   2:Version2比較新</returns>
        private static int WhitchIsNew(string Version1, string Version2)
        {
            var matches_version1 = regex_getVersion.Matches(Version1);
            var matches_version2 = regex_getVersion.Matches(Version2);
            var list = matches_version1.Cast<Match>().Select(match => match.Value).ToList();
            var list2 = matches_version2.Cast<Match>().Select(match => match.Value).ToList();

            //當版本號分割出來的list長度不一樣時，由於無法判定，因此直接視為新版
            if (list.Count != list2.Count)
                return -1;
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    List<string> temp1 = new List<string>(list[i].TrimEnd('.').Split('.'));
                    List<string> temp2 = new List<string>(list2[i].TrimEnd('.').Split('.'));
                    int max = Math.Max(temp1.Count, temp2.Count);


                    for (int j = 0; j < max; j++)
                    {
                        int difference = (j >= temp1.Count ? 0 : int.Parse(temp1[j])) - (j >= temp2.Count ? 0 : int.Parse(temp2[j]));
                        if (difference > 0)
                            return 1;
                        else if (difference < 0)
                            return 2;
                    }
                }
            }
            return 0;
        }
        private static string GetVersionFromMod(string zipFile)
        {
            Regex regex_version = new Regex("\"version\"\\s*:\\s*\"(.*?)\"", RegexOptions.IgnoreCase);
            using (ZipFile zip = new ZipFile(zipFile))
            {
                foreach (ZipEntry entry in zip)
                {
                    if (entry.IsFile)
                    {
                        if (entry.Name.ToLower() == "mcmod.info" || entry.Name.ToLower() == "litemod.json")
                        {
                            using (Stream zipStream = zip.GetInputStream(zip.GetEntry(entry.Name)))
                            {
                                // Manipulate the output filename here as desired.
                                using (StreamReader StreamReader_mcmod_info = new StreamReader(zipStream, Encoding.UTF8))
                                {
                                    string info = StreamReader_mcmod_info.ReadToEnd();
                                    string version = regex_version.Match(info).Groups[1].ToString();
                                    return version;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }


        public class Class_VersionFilter
        {
            public string Version { get; set; }
            public string value { get; set; }
        }
        public class Class_Version
        {
            public string Name { get; set; }
            public ReleaseStats Stats { get; set; }
            public string Href { get; set; }
        }

        public enum ReleaseStats
        {
            Error,
            Alpha,
            Beta,
            Release
        }
    }
}
