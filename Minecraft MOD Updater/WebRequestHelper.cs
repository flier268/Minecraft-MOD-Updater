﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Minecraft_MOD_Updater
{
    public class WebRequestHelper
    {
        /// <summary>
        /// 從轉跳的網址取得檔案名稱
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="MaximumAutomaticRedirections">轉址層數</param>
        /// <returns></returns>
        public static async Task<string> GetTransferedURLAsync(string URL, int MaximumAutomaticRedirections)
        {
            string redirectUrl = URL;
            HttpWebRequest request;
            HttpWebResponse response;
            for (int i = 0; i < MaximumAutomaticRedirections; i++)
            {
                request = (HttpWebRequest)WebRequest.Create(redirectUrl);
                request.Referer = URL;
                request.AllowAutoRedirect = false;
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36";
                response = (HttpWebResponse)await request.GetResponseAsync();                
                redirectUrl = response.Headers["Location"];
            }
            return redirectUrl;
        }        
    }
}
