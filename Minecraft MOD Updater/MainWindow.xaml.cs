﻿using ICSharpCode.SharpZipLib.Zip;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using static Minecraft_MOD_Updater.CurseHelper;

namespace Minecraft_MOD_Updater
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        List<Class_VersionFilter> class_VersionFilters = new List<Class_VersionFilter>();
        const int Thread_Count = 3;
        public MainWindow()
        {
            this.DataContext = this;
            GridItems = new ObservableCollection<GridItem>();
            disableList = new ObservableCollection<string>();
            App.ReloadSetting();
            ManuallyMCVersion = App.Settings.General.ManuallyMcVersion;
            AutoMCVersion = !ManuallyMCVersion;
            MCVersion = App.Settings.General.McVersion;
            Path_MODS = App.Settings.General.ModsPath;
            AllowLowVersion = App.Settings.General.AllowLowVersion;
            InitializeComponent();
        }
        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        
        public bool ManuallyMCVersion { get; set; }
        public bool AutoMCVersion { get; set; }
        public string MCVersion { get; set; }
        public bool AllowLowVersion { get; set; }
        private bool _CheckSelected_Enable = true;
        public bool CheckSelected_Enable { get=> _CheckSelected_Enable; set { _CheckSelected_Enable = value;OnPropertyChanged("CheckSelected_Enable"); } }
        private bool _DownloadSelected_Enable = true;
        public bool DownloadSelected_Enable { get => _DownloadSelected_Enable; set { _DownloadSelected_Enable = value; OnPropertyChanged("DownloadSelected_Enable"); } }
        
        public ObservableCollection<string> _disableList;
        public ObservableCollection<string> disableList { get => _disableList; set { _disableList = value;OnPropertyChanged("disableList"); } }
        private ObservableCollection<GridItem> _GridItems;
        public ObservableCollection<GridItem> GridItems { get => _GridItems; set { _GridItems = value;OnPropertyChanged("GridItems"); } }
        Regex regex_getVersion = new Regex(@"(?:(\d+)\.*)+");
        string[] type = { "*.jar", "*.zip", "*.litemod" };
        private string _Path_MODS;
        public string Path_MODS
        {
            get => _Path_MODS;
            set
            {
                _Path_MODS = value;
                OnPropertyChanged("Path_MODS");
                if (Directory.Exists(_Path_MODS))
                {
                    if (watch != null)
                    {
                        watch.Created -= Watch_Changed;
                        watch.Changed -= Watch_Changed;
                        watch.Deleted -= Watch_Changed;
                    }
                    watch = new FileSystemWatcher(_Path_MODS, "*.*");
                    //開啟監聽
                    watch.EnableRaisingEvents = true;
                    //是否連子資料夾都要偵測
                    watch.IncludeSubdirectories = true;
                    //新增時觸發事件
                    watch.Created += Watch_Changed;
                    watch.Changed += Watch_Changed;
                    watch.Deleted += Watch_Changed;
                }
            }
        }
        private void ReloadMODSList()
        {
            foreach (var griditem in GridItems)
                griditem.Remove = true;
            Regex regex_mcversion = new Regex("\"mcversion\"\\s*:\\s*\"(.*?)\"", RegexOptions.IgnoreCase);
            Regex regex_version = new Regex("\"version\"\\s*:\\s*\"(.*?)\"", RegexOptions.IgnoreCase);
            Regex regex_name = new Regex("\"name\"\\s*:\\s*\"(.*?)\"", RegexOptions.IgnoreCase);
            foreach (string _type in type)
            {
                string[] fileList = Directory.GetFiles(Path_MODS, _type, SearchOption.AllDirectories);
                foreach (string zipFile in fileList)
                {
                    if (!File.Exists(zipFile))
                        continue;

                    using (ZipFile zip = new ZipFile(zipFile))
                    {
                        foreach (ZipEntry entry in zip)
                        {
                            if (entry.IsFile)
                            {
                                if (entry.Name.ToLower() == "mcmod.info" || entry.Name.ToLower() == "litemod.json")
                                {
                                    Stream zipStream = zip.GetInputStream(zip.GetEntry(entry.Name));
                                    // Manipulate the output filename here as desired.
                                    using (StreamReader StreamReader_mcmod_info = new StreamReader(zipStream, Encoding.UTF8))
                                    {
                                        string info = StreamReader_mcmod_info.ReadToEnd();
                                        string mcversion = regex_mcversion.Match(info).Groups[1].ToString();
                                        string version = regex_version.Match(info).Groups[1].ToString();
                                        string name = regex_name.Match(info).Groups[1].ToString();
                                        var temp = GridItems.FirstOrDefault(x => x.ModName == name && x.CurrentVersion == version);
                                        if (temp == new GridItem() || temp == null)
                                            GridItems.Add(new GridItem() { Enable = true, CurrentVersion = version, FileName = zipFile, MCVersion = mcversion, ModName = name, CurseModName = name, Stats = "Not Check", Remove = false });
                                        else
                                        {
                                            temp.CurrentVersion = version;
                                            temp.MCVersion = mcversion;
                                            temp.Remove = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ObservableCollection<GridItem> gridItems_temp = new ObservableCollection<GridItem>();
            GridItems.Select(x => x.ModName).Distinct().ToList().ForEach(x => gridItems_temp.Add(GridItems.Where(y => y.ModName == x).FirstOrDefault()));
            GridItems = gridItems_temp;
            ReloadDisableList();
            var gridItems= GridItems.Where(x => !x.Remove).ToList();
            GridItems = new ObservableCollection<GridItem>(gridItems);
        }
        private void ReloadDisableList()
        {
            disableList.Clear();
            Directory.GetFiles(Path_MODS, "*.disable", SearchOption.AllDirectories).ToList().ForEach(x => disableList.Add(x));            
        }
        FileSystemWatcher watch;
        private void Button_SelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var griditem in GridItems)
                griditem.Enable = true;
        }
        private void Button_UnselectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var griditem in GridItems)
                griditem.Enable = false;
        }


        private void Button_CheckSelected_Click(object sender, RoutedEventArgs e)
        {
            Task task = Task.Run(() => CheckSelected());
        }      
        private void CheckSelected()
        {
            CheckSelected_Enable = false;
            var gridItems = DataGrid_List.Items;
            int count = 0;
            for (int i = 0; i < gridItems.Count; i++) if ((gridItems[i] as GridItem).Enable) count++;
            
            List<GridItem>  items  = new List<GridItem>();
            List<Task> t = new List<Task>();
            for (int i = 0; i < gridItems.Count; i++)
            {
                if ((gridItems[i] as GridItem).Enable)
                {
                    items.Add((gridItems[i] as GridItem));
                }
                if(items.Count==count/Thread_Count|| i==gridItems.Count-1)
                {
                    GridItem[] temp = new GridItem[items.Count];
                    items.CopyTo(temp);
                    t.Add(Task.Run(() => Check(temp)));
                    items.Clear();
                }
            }
            t.ForEach(x => x.Wait());
            CheckSelected_Enable = true;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                DataGrid_List.Items.SortDescriptions.Clear();
                DataGrid_List.Items.SortDescriptions.Add(new SortDescription { PropertyName = "Enable", Direction = ListSortDirection.Descending });
                DataGrid_List.Items.SortDescriptions.Add(new SortDescription { PropertyName = "ModName", Direction = ListSortDirection.Ascending });
            }));
        }
        private void Check(GridItem[] gridItems)
        {
            foreach (var gridItem in gridItems)
                try
                {
                    CheckVerstion(gridItem);
                }
                catch (Exception e) { }
        }
        private void Button_DownloadSelected_Click(object sender, RoutedEventArgs e)
        {
            var task=Task.Run(() => DownloadSelected());
        }
        private void DownloadSelected()
        {
            watch.Created -= Watch_Changed;
            watch.Changed -= Watch_Changed;
            watch.Deleted -= Watch_Changed;
            DownloadSelected_Enable = false;
           
            var gridItems = DataGrid_List.Items;
            int count = 0;
            for (int i = 0; i < gridItems.Count; i++) if ((gridItems[i] as GridItem).Enable) count++;
            
            List<GridItem> items = new List<GridItem>();
            List<Task> t = new List<Task>();       
            for (int i = 0; i < gridItems.Count; i++)
            {
                if ((gridItems[i] as GridItem).Enable)
                {
                    items.Add((gridItems[i] as GridItem));
                }
                if (items.Count == count / 1 || i == gridItems.Count-1)
                {
                    GridItem[] temp = new GridItem[items.Count];
                    items.CopyTo(temp);                   
                    t.Add(Task.Run(() => DownloadAsync(temp)));
                    items.Clear();
                }
            }
            t.ForEach(x => x.Wait());


           
            Dispatcher.BeginInvoke(new Action(() =>
            {
                ReloadMODSList();
                DownloadSelected_Enable = true;
            }));
            
            watch.Created += Watch_Changed;
            watch.Changed += Watch_Changed;
            watch.Deleted += Watch_Changed;
        }
        private async Task DownloadAsync(GridItem[] gridItems)
        {
            foreach (var gridItem in gridItems.Where(x => x.Company != null).ToList())
            {
                try
                {
                    await DownloadOneAsync(gridItem);
                }
                catch (Exception e) { System.Diagnostics.Debug.Print(e.Message); }
            }
        }
        private void Button_Browse_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.Title = "Select mods folder";
            dlg.IsFolderPicker = true;
            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = false;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Path_MODS = dlg.FileName;
                ReloadMODSList();
            }
        }

        private void Watch_Changed(object sender, FileSystemEventArgs e)
        {
            //ReloadMODSList();
        }

        private void CheckVerstion(GridItem gridItem)
        {
            if (gridItem == null)
                return;
            string _MCVerstion = ManuallyMCVersion ? MCVersion : gridItem.MCVersion;
            if (string.IsNullOrEmpty(_MCVerstion))
            {
                string modname_temp = gridItem.ModName.Replace(" ", "").Replace("_", "").Replace("'", "").ToLower();
                string currentVersion_temp = gridItem.CurrentVersion.Contains("-") ? gridItem.CurrentVersion.Split('-')[1] : gridItem.CurrentVersion;
                _MCVerstion = Path.GetFileNameWithoutExtension(gridItem.FileName)
                    .Replace(" ", "").Replace("_", "").Replace("'", "").ToLower()
                    .Replace(modname_temp, "")
                    .Replace(currentVersion_temp, "")
                    .Replace("-", "").Replace("mc", "");
                if (_MCVerstion == "" || !class_VersionFilters.Select(x => x.Version).Contains(_MCVerstion))
                {
                    gridItem.HaveNewVersion = false;
                    gridItem.Stats = "Not Support.";
                    gridItem.Enable = false;
                    return;
                }
            }
            if (!class_VersionFilters.Select(x => x.Version).Contains(_MCVerstion))
            {
                var temp = CurseHelper.GetVersionFilterListAsync(gridItem.ModName);
                if (temp == null)
                {
                    gridItem.HaveNewVersion = false;
                    gridItem.Stats = "Not Support.";
                    gridItem.Enable = false;
                    return;
                }
                temp.Result.ForEach(x =>
                {
                    if (class_VersionFilters.Where(y => y.Version == x.Version).Count() == 0)
                        class_VersionFilters.Add(x);
                });
            }
            var VersionFilterListTemp = class_VersionFilters.Where(x => x.Version == _MCVerstion);
            string lowerversion = _MCVerstion;
            while (VersionFilterListTemp.Count() == 0 && AllowLowVersion && lowerversion != null)
            {
                lowerversion = PrivateFunction.GetLowerVersion(lowerversion);
                VersionFilterListTemp = class_VersionFilters.Where(x => x.Version == lowerversion);
            }
            string value = class_VersionFilters.Where(x => x.Version == lowerversion).First().value;

            var VersionList = CurseHelper.GetFileListByFilterAsync(gridItem.CurseModName, new CurseHelper.Class_VersionFilter() { Version = lowerversion, value = value });
            if (VersionList.Result == null && AllowLowVersion)
            {
                while (lowerversion !=null && VersionList.Result ==null)
                {
                    lowerversion = PrivateFunction.GetLowerVersion(lowerversion);
                    value = class_VersionFilters.Where(x => x.Version == lowerversion).First().value;
                    VersionList = CurseHelper.GetFileListByFilterAsync(gridItem.CurseModName, new CurseHelper.Class_VersionFilter() { Version = lowerversion, value = value });
                }
            }
            ObservableCollection<VersionListItem> companies = new ObservableCollection<VersionListItem>();
            int id = 0;
            if (VersionList == null)
            {
                gridItem.HaveNewVersion = false;
                gridItem.Stats = "Not Support.";
                gridItem.Enable = false;
                return;
            }
            VersionList.Result.ForEach(x =>
            {
                companies.Add(new VersionListItem() { ID = id++, Name = x.Stats.ToString() + ": " + x.Name, Stats = x.Stats, Href = x.Href });
            });
            gridItem.VersionList = companies;
            gridItem.Company = companies[0];

            type.ToList().ForEach(x => VersionList.Result[0].Name = VersionList.Result[0].Name.Replace(x.Replace("*", ""), ""));
            var matches= regex_getVersion.Matches(VersionList.Result[0].Name);
            var matches_currentversion = regex_getVersion.Matches(gridItem.CurrentVersion);

            var list = matches.Cast<Match>().Select(match => match.Value).ToList();
            var list2 = matches_currentversion.Cast<Match>().Select(match => match.Value).ToList();
            gridItem.Enable = false;
            gridItem.HaveNewVersion = false;
            gridItem.Stats = "Version is same";
            list2.ForEach(
                x =>
                {
                    if (!list.Contains(x))
                    {
                        gridItem.Stats = "Version not same";
                        gridItem.Enable = true;
                        gridItem.HaveNewVersion = true;
                    }
                });      
        }
        private async Task DownloadOneAsync(GridItem gridItem)
        {
            if (gridItem == null)
                return;
            string lastversion_DownloadLink = Curseforge_URL + gridItem.VersionList.First(x => x.ID == gridItem.Company.ID).Href;
            if (File.Exists(gridItem.FileName))
                await CurseHelper.DownloadsAsync(lastversion_DownloadLink, gridItem.FileName, gridItem.CurrentVersion, true);
        }
        
        private void Button_DeleteDisableFile_Click(object sender, RoutedEventArgs e)
        {
            DownloadSelected_Enable = true;
            string[] fileList = Directory.GetFiles(Path_MODS, "*.disable", SearchOption.AllDirectories);
            foreach (string path in fileList)
                try
                {
                    File.Delete(path);
                }
                catch { MessageBox.Show(string.Format("Delete {0} Fail", path)); break; }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            App.Settings.General.ManuallyMcVersion = ManuallyMCVersion;
            App.Settings.General.McVersion = MCVersion;
            App.Settings.General.ModsPath = Path_MODS;
            App.Settings.General.AllowLowVersion = AllowLowVersion;
            App.SaveSetting();
        }
    }
    public class GridItem : INotifyPropertyChanged
    {
        private bool _Enable;
        public bool Enable { get => _Enable; set { _Enable = value;OnPropertyChanged("Enable"); } }
        /// <summary>
        /// ReloadMODSList會先把值設為True，讀取mod的訊息文件後會設為False，最後如果值還是True的則移除
        /// </summary>
        public bool Remove { get; set; }
        private string _ModName;
        public string ModName { get => _ModName; set { _ModName = value; OnPropertyChanged("ModName"); } }
        public string CurseModName { get; set; }
        private string _CurrentVersion;
        public string CurrentVersion { get => _CurrentVersion; set { _CurrentVersion = value;OnPropertyChanged("CurrentVersion"); } }


        public string FileName { get; set; }
        private string _MCVersion;
        public string MCVersion { get => _MCVersion; set { _MCVersion = value;OnPropertyChanged("MCVersion"); } }
        public string Stats { get; set; }
        public bool HaveNewVersion { get; set; }
        public VersionListItem Company { get => _Company; set { _Company = value;OnPropertyChanged("Company"); } }
        public IEnumerable<VersionListItem> VersionList { get => _VersionList; set { _VersionList = value;OnPropertyChanged("VersionList"); } }


        private VersionListItem _Company { get; set; }
        private IEnumerable<VersionListItem> _VersionList;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class VersionListItem
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public override string ToString() { return Name; }

        public ReleaseStats Stats { get; set; }
        public string Href { get; set; }
    }
}
