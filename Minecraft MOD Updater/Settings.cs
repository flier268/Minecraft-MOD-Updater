﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace Minecraft_MOD_Updater
{
    public partial class Settings
    {
        public Settings() { General = new General(); }
        [JsonProperty("General")]
        public General General { get; set; }
    }

    public partial class General
    {
        public General() { ModsPath = "";ManuallyMcVersion = true;McVersion = "1.12.2"; AllowLowVersion = true; }
        [JsonProperty("ModsPath")]
        public string ModsPath { get; set; }

        [JsonProperty("ManuallyMCVersion")]
        public bool ManuallyMcVersion { get; set; }

        [JsonProperty("MCVersion")]
        public string McVersion { get; set; }

        [JsonProperty("AllowLowVersion")]
        public bool AllowLowVersion { get;  set; }
    }

    public partial class Settings
    {
        public static Settings FromJson(string json) => JsonConvert.DeserializeObject<Settings>(json, Minecraft_MOD_Updater.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Settings self) => JsonConvert.SerializeObject(self,Formatting.Indented, Minecraft_MOD_Updater.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
