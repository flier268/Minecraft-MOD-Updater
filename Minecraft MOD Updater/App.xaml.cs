﻿using System;
using System.IO;
using System.Text;
using System.Windows;

namespace Minecraft_MOD_Updater
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        private readonly static string jsonPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config.json");
        public static Settings Settings = new Settings();
        public static void ReloadSetting()
        {
            if (File.Exists(jsonPath))
            {
                using (StreamReader sr = new StreamReader(jsonPath, Encoding.UTF8))
                {
                    Settings = Settings.FromJson(sr.ReadToEnd());
                }
            }
        }
        public static void SaveSetting()
        {
            using (StreamWriter sw = new StreamWriter(jsonPath, false, Encoding.UTF8))
            {
                sw.Write(Settings.ToJson());
                sw.Flush();
            }            
        }
    }
}
