﻿using System.Linq;
using System.Text;

namespace Minecraft_MOD_Updater
{
    public class PrivateFunction
    {
        internal static string GetLowerVersion(string _MCVerstion)
        {
            string[] array = _MCVerstion.Split('.');
            StringBuilder stringBuilder = new StringBuilder();
            if (array.Length >= 3)
            {
                array[array.Length - 1] = (int.Parse(array[array.Length - 1]) - 1).ToString();
                if (array[array.Length - 1] != "-1")
                    array.ToList().ForEach(x => stringBuilder.AppendFormat("{0}{1}", (stringBuilder.Length == 0 ? "" : "."), x));
                if (stringBuilder.ToString().EndsWith(".0"))
                {
                    string temp= stringBuilder.ToString().Substring(0, stringBuilder.Length - 2);
                    stringBuilder.Clear();
                    stringBuilder.Append(temp);                    
                }                    
            }
            else
                return null;
            return stringBuilder.ToString();
        }
    }
}
