﻿//Copy from https://gist.github.com/subena22jf/3358b8609966203502a5
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Minecraft_MOD_Updater
{
    public class Downloader
    {
        public static async Task Download(string url, string saveAs)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
            var parallelDownloadSuported = response.Headers.AcceptRanges.Contains("bytes");
            var contentLength = response.Content.Headers.ContentLength ?? 0;

            if (parallelDownloadSuported)
            {
                const double numberOfParts = 5.0;
                var tasks = new List<Task>();
                var partSize = (long)Math.Ceiling(contentLength / numberOfParts);

                File.Create(saveAs).Dispose();

                for (var i = 0; i < numberOfParts; i++)
                {
                    var start = i * partSize + Math.Min(1, i);
                    var end = Math.Min((i + 1) * partSize, contentLength);

                    tasks.Add(
                        Task.Run(() => DownloadPart(url, saveAs, start, end))
                        );
                }
                await Task.WhenAll(tasks);
            }
            else
            {
                using (var webclient = new WebClient())
                {
                    try
                    {
                        webclient.DownloadFileTaskAsync(url, saveAs).Wait(20000);
                    }
                    catch (Exception e) { }
                }                
            }
        }
        private static void DownloadAll(string url, string saveAs)
        {
            var webclient = new WebClient();
            try
            {
                webclient.DownloadFileTaskAsync(url, saveAs).Wait();
            }
            catch (Exception e) { }
            /*
            var webRequest = WebRequest.CreateHttp(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)(await webRequest.GetResponseAsync());
                BinaryReader bin = new BinaryReader(response.GetResponseStream());
                byte[] buffer = bin.ReadBytes((Int32)response.ContentLength);
                using (Stream writer = File.Create(saveAs))
                {
                    await writer.WriteAsync(buffer, 0, buffer.Length);
                    await writer.FlushAsync();
                }
            }
            catch (Exception e) { }*/
        }
        private static async void DownloadPart(string url, string saveAs, long start, long end)
        {
            using (var httpClient = new HttpClient())
            using (var fileStream = new FileStream(saveAs, FileMode.Open, FileAccess.Write, FileShare.Write))
            {
                var message = new HttpRequestMessage(HttpMethod.Get, url);
                message.Headers.Add("Range", string.Format("bytes={0}-{1}", start, end));

                fileStream.Position = start;
                await httpClient.SendAsync(message).Result.Content.CopyToAsync(fileStream);
            }
        }
    }
}
